FROM golang:1.21 AS coin-tracker-build
# Set destination for COPY
WORKDIR /build
# Download Go modules
COPY go.mod go.sum ./
RUN go mod download
# Copy the source code
COPY . ./
# Build
RUN CGO_ENABLED=0 GOOS=linux go build -o /build/coin-tracker


# Run the tests in the container
FROM coin-tracker-build AS coin-tracker-test
RUN go test -v ./...


# Deploy the application binary into a lean image
FROM gcr.io/distroless/base-debian11 AS coin-tracker-release
WORKDIR /app
COPY --from=coin-tracker-build /build/coin-tracker ./coin-tracker
COPY app.yaml ./app.yaml
USER nonroot:nonroot
ENTRYPOINT ["/app/coin-tracker"]