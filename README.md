# Coin Tracker

This is a test project for retrieving the latest prices of currencies available in exchanges.

## Requirement

- GoLang
- MongoDB
- RabbitMQ
- Docker

## Architecture

The project architecture is as follows:
<br />
<img src="coin-tracker-architecture-v1.png" align="center" />

The project workflow operates as follows:

- The project operates by sending an HTTP request to obtain a list of currencies supported by the exchange.
- Subsequently, the currencies are stored in a MongoDB database for future use.
- Then, another HTTP request is sent to KuCoin to obtain a token, and subsequently, with the received token,
  a WebSocket connection is established.
- After establishing the connection, considering the ping interval specified by the exchange in the token request,
  a ping request is regularly sent within the ping interval to keep the connection alive. Due to the limitation of
  300 subscribes per session, symbols requiring K-line data for all timeframes are batch-subscribed
  (up to a maximum of 100 topics in a batch subscribe).
- In case of a disconnection, a mechanism is in place to re-establish the connection, and due to unsubscribe of
  all symbols in the new connection, all the symbols needed are subscribed again.
- Upon receiving each message through the WebSocket, the received message, based on the topic type, is published on
  a RabbitMQ exchange with a routing key pattern of:

```json lines
{symbol}.{type}
```

- Each client can create a queue based on its specific needs and bind it to an exchange with the required routing key.
  For example, it can use patterns like:

```text
BTC-USDT.*
BTC-USDT.1min
BTC-USDT.#
```

### Stack

- MongoDB database has been utilized for this project due to its high insertion speed and the lack of a schema
  requirement. (In case of inserting K-lines into the database)
- Due to the need to maintain the order of messages, as well as the mentioned capability, RabbitMQ has been chosen
  for this project.
- In this project, the multi-threading capability, as well as the goroutine feature present in the Go language,
  has been utilized.

### Limitation

- Number of Connections Number of connections per user ID: ≤ 50
- Connection Times Connection Limit: 30 per minute
- Number of Uplink Messages Message limit sent to the server: 100 per 10 seconds
- Topic Subscription Limit Maximum number of batch subscriptions at a time: 100 topics
- Subscription limit for each connection: 300 topics (There is no such restriction in the futures)
- List of all k-line time interval:

```json lines
1min, 3min, 5min, 15min, 30min, 1hour, 4hour, 6hour, 8hour, 12hour, 1day, 1week
```

You can view all the constraints for connecting to the KuCoin exchange
WebSocket [at this link](https://www.kucoin.com/docs/basic-info/request-rate-limit/websocket).

## Build and Run

To execute the project, you need to first install ``Docker`` on your system. Then, follow these commands:
```shell
cd <project_dir>
docker compose up -d
```
