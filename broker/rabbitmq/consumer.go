package rabbitmq

import (
	"os"
	"time"

	amqp "github.com/rabbitmq/amqp091-go"
	"github.com/rs/zerolog"
)

// Consumer for receiving AMPQ events
type Consumer struct {
	conn      *amqp.Connection
	queueName string
	Logger    *zerolog.Logger
}

func (consumer *Consumer) setup() error {
	l := zerolog.New(
		zerolog.ConsoleWriter{Out: os.Stdout, TimeFormat: time.DateTime},
	).Level(zerolog.InfoLevel).With().Caller().Timestamp().Logger()
	consumer.Logger = &l

	channel, err := consumer.conn.Channel()
	if err != nil {
		return err
	}
	return declareExchange(channel)
}

// NewConsumer returns a new Consumer
func NewConsumer(conn *amqp.Connection) (Consumer, error) {
	consumer := Consumer{
		conn: conn,
	}
	err := consumer.setup()
	if err != nil {
		return Consumer{}, err
	}

	return consumer, nil
}

// Listen will listen for all new Queue publications
func (consumer *Consumer) Listen(name string, topics []string) error {
	ch, err := consumer.conn.Channel()
	if err != nil {
		return err
	}
	defer func(ch *amqp.Channel) {
		_ = ch.Close()
	}(ch)

	q, err := declareRandomQueue(name, ch)
	if err != nil {
		return err
	}

	for _, s := range topics {
		err = ch.QueueBind(
			q.Name,
			s,
			getExchangeName(),
			false,
			nil,
		)

		if err != nil {
			return err
		}
	}

	messages, err := ch.Consume(q.Name, "", true, false, false, false, nil)
	if err != nil {
		return err
	}

	forever := make(chan bool)
	go func() {
		for d := range messages {
			consumer.Logger.Info().Msgf("Received a message: %s", d.Body)
		}
	}()

	consumer.Logger.Info().Msgf("Waiting for message Exchange: %s, Queue: %s", getExchangeName(), q.Name)
	<-forever
	return nil
}
