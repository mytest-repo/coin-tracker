package rabbitmq

import (
	"context"
	"os"
	"time"

	amqp "github.com/rabbitmq/amqp091-go"
	"github.com/rs/zerolog"
)

// Emitter for publishing AMQP events
type Emitter struct {
	connection *amqp.Connection
	Logger     *zerolog.Logger
}

func (e *Emitter) setup() error {
	l := zerolog.New(
		zerolog.ConsoleWriter{Out: os.Stdout, TimeFormat: time.DateTime},
	).Level(zerolog.InfoLevel).With().Caller().Timestamp().Logger()
	e.Logger = &l

	channel, err := e.connection.Channel()
	if err != nil {
		panic(err)
	}

	defer func(channel *amqp.Channel) {
		_ = channel.Close()
	}(channel)
	return declareExchange(channel)
}

// Push (Publish) a specified message to the AMQP exchange
func (e *Emitter) Push(event string, key string) error {
	channel, err := e.connection.Channel()
	if err != nil {
		return err
	}

	defer func(channel *amqp.Channel) {
		_ = channel.Close()
	}(channel)

	err = channel.PublishWithContext(context.Background(),
		getExchangeName(),
		key,
		false,
		false,
		amqp.Publishing{
			ContentType: "application/json",
			Body:        []byte(event),
		},
	)
	e.Logger.Debug().Msgf("Sending message: %s -> %s", event, getExchangeName())
	return nil
}

// NewEventEmitter returns a new rabbitmq.Emitter object
// ensuring that the object is initialised, without error
func NewEventEmitter(conn *amqp.Connection) (Emitter, error) {
	emitter := Emitter{
		connection: conn,
	}

	err := emitter.setup()
	if err != nil {
		return Emitter{}, err
	}

	return emitter, nil
}
