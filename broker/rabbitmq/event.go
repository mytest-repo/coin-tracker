package rabbitmq

import (
	amqp "github.com/rabbitmq/amqp091-go"
	"github.com/spf13/viper"
)

func getExchangeName() string {
	return viper.GetString("rabbitmq.exchangeName")
}

func declareRandomQueue(name string, ch *amqp.Channel) (amqp.Queue, error) {
	return ch.QueueDeclare(
		name,
		false,
		false,
		true,
		false,
		nil,
	)
}

func declareExchange(ch *amqp.Channel) error {
	return ch.ExchangeDeclare(
		getExchangeName(),
		"topic",
		true,
		false,
		false,
		false,
		nil,
	)
}
