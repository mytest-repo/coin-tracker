package kucoin

import (
	"encoding/json"
	"io"
	"net/http"
	"net/url"
	"os"
	"time"

	"github.com/rs/zerolog"
	"github.com/spf13/viper"
)

type GetSymbolsRes struct {
	Code string `json:"code"`
	Data []struct {
		Symbol          string `json:"symbol"`
		Name            string `json:"name"`
		BaseCurrency    string `json:"baseCurrency"`
		QuoteCurrency   string `json:"quoteCurrency"`
		FeeCurrency     string `json:"feeCurrency"`
		Market          string `json:"market"`
		BaseMinSize     string `json:"baseMinSize"`
		QuoteMinSize    string `json:"quoteMinSize"`
		BaseMaxSize     string `json:"baseMaxSize"`
		QuoteMaxSize    string `json:"quoteMaxSize"`
		BaseIncrement   string `json:"baseIncrement"`
		QuoteIncrement  string `json:"quoteIncrement"`
		PriceIncrement  string `json:"priceIncrement"`
		PriceLimitRate  string `json:"priceLimitRate"`
		MinFunds        string `json:"minFunds"`
		IsMarginEnabled bool   `json:"isMarginEnabled"`
		EnableTrading   bool   `json:"enableTrading"`
	}
}

type Candle struct {
	Topic   string `json:"topic"`
	Type    string `json:"type"`
	Subject string `json:"subject"`
	Data    struct {
		Symbol  string   `json:"symbol"`
		Candles []string `json:"candles"`
		Time    int64    `json:"time"`
	} `json:"data"`
}

type GetTokenRes struct {
	Code string `json:"code"`
	Data struct {
		Token           string `json:"token"`
		InstanceServers []struct {
			Endpoint     string `json:"endpoint"`
			Encrypt      bool   `json:"encrypt"`
			Protocol     string `json:"protocol"`
			PingInterval int    `json:"pingInterval"`
			PingTimeout  int    `json:"pingTimeout"`
		} `json:"instanceServers"`
	} `json:"data"`
}

type ResError struct {
	Code    string `json:"code"`
	Msg     string `json:"msg"`
	Retry   bool   `json:"retry"`
	Success bool   `json:"success"`
}

type RestApi struct {
	BaseUrl *url.URL
	Logger  *zerolog.Logger
}

func (e *ResError) Error() string {
	return e.Msg
}

func (ra *RestApi) Init() *RestApi {
	l := zerolog.New(
		zerolog.ConsoleWriter{Out: os.Stdout, TimeFormat: time.DateTime},
	).Level(zerolog.InfoLevel).With().Caller().Timestamp().Logger()
	ra.Logger = &l

	ra.BaseUrl, _ = url.Parse(viper.GetString("kucoin.baseUrl"))

	return ra
}

func (ra *RestApi) getUrl(path string) *url.URL {
	u := ra.BaseUrl
	u.Path = path

	return u
}

func (ra *RestApi) GetAllSymbol() (GetSymbolsRes, error) {
	u := ra.getUrl("/api/v2/symbols")
	res, err := http.Get(u.String())
	if err != nil {
		ra.Logger.Error().Err(err).Msgf("Error in kucoin client GetAllSymbol: %s", err)
	}
	defer func(Body io.ReadCloser) {
		_ = Body.Close()
	}(res.Body)
	var jsonRes GetSymbolsRes

	if res.StatusCode == http.StatusOK {
		bodyBytes, err := io.ReadAll(res.Body)
		if err != nil {
			ra.Logger.Error().Err(err).Msgf("Error in kucoin client GetAllSymbol: %s", err)
		}
		err = json.Unmarshal(bodyBytes, &jsonRes)

		return jsonRes, nil
	} else {
		bodyBytes, err := io.ReadAll(res.Body)
		if err != nil {
			ra.Logger.Error().Err(err).Msgf("Error in kucoin client GetAllSymbol: %s", err)
		}
		var resErr ResError
		err = json.Unmarshal(bodyBytes, &resErr)
		return jsonRes, &resErr
	}
}

func (ra *RestApi) GetWebsocketToken() (GetTokenRes, error) {
	u := ra.getUrl("/api/v1/bullet-public")
	res, err := http.Post(u.String(), "application/json", nil)
	if err != nil {
		ra.Logger.Error().Err(err).Msgf("Error in kucoin client GetWebsocketToken: %s", err)
	}
	defer func(Body io.ReadCloser) {
		_ = Body.Close()
	}(res.Body)
	var result GetTokenRes

	if res.StatusCode == http.StatusOK {
		bodyBytes, err := io.ReadAll(res.Body)
		if err != nil {
			ra.Logger.Error().Err(err).Msgf("Error in kucoin client GetWebsocketToken: %s", err)
		}
		var result GetTokenRes
		err = json.Unmarshal(bodyBytes, &result)

		return result, nil
	} else {
		bodyBytes, err := io.ReadAll(res.Body)
		if err != nil {
			ra.Logger.Error().Err(err).Msgf("Error in kucoin client GetWebsocketToken: %s", err)
		}
		var resErr ResError
		err = json.Unmarshal(bodyBytes, &resErr)
		return result, &resErr
	}
}
