package config

type Application struct {
	Rabbitmq Rabbitmq `mapstructure:"rabbitmq"`
	Mongodb  Mongodb  `mapstructure:"mongodb"`
	Kucoin   Kucoin   `mapstructure:"kucoin"`
}
