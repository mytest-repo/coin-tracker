package config

type Kucoin struct {
	SocketId       string `mapstructure:"socketId"`
	BaseUrl        string `mapstructure:"baseUrl"`
	SubscribeDelay int    `mapstructure:"subscribeDelay"`
}
