package config

type Rabbitmq struct {
	Host         string `mapstructure:"host"`
	Port         int    `mapstructure:"port"`
	User         string `mapstructure:"user"`
	Password     string `mapstructure:"password"`
	ExchangeName string `mapstructure:"exchangeName"`
}
