package main

import (
	"cointracker/config"
	"cointracker/service"
	"github.com/spf13/viper"
	"strings"
	"time"
)

func main() {
	setup()

	kucoinService := new(service.KucoinService).Init()
	kucoinService.Run()
}

func setup() {
	_, err := LoadConfig(".")
	if err != nil {
		panic(err)
	}

	time.Sleep(time.Second * viper.GetDuration("app.delay"))
}

func LoadConfig(path string) (config *config.Application, err error) {
	viper.AddConfigPath(path)
	viper.SetConfigName("config")
	viper.SetConfigName("app")
	viper.SetConfigType("yaml")
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	viper.AutomaticEnv()

	err = viper.ReadInConfig()
	if err != nil {
		return
	}

	err = viper.Unmarshal(&config)
	return
}
