package model

type Candle struct {
	Model
	Interval string  `bson:"interval"`
	Symbol   string  `bson:"symbol"`
	Time     int     `bson:"time"`
	Open     float64 `bson:"open"`
	Close    int     `bson:"close"`
	High     int     `bson:"high"`
	Low      int     `bson:"low"`
	Volume   float64 `bson:"volume"`
	Turnover float64 `bson:"turnover"`
}
