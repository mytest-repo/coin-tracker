package model

import (
	"context"
	"github.com/rs/zerolog"
	"os"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type ORM struct {
	Logger *zerolog.Logger
	model  *Model
	db     *mongo.Database
}

func (o *ORM) Connect(uri string) (*mongo.Client, error) {
	l := zerolog.New(
		zerolog.ConsoleWriter{Out: os.Stdout, TimeFormat: time.DateTime},
	).Level(zerolog.InfoLevel).With().Caller().Timestamp().Logger()
	o.Logger = &l

	clientOptions := options.Client().ApplyURI(uri)
	client, err := mongo.Connect(context.Background(), clientOptions)
	if err != nil {
		o.Logger.Error().Err(err).Msgf("MongoDB connection error: %s", err)
	}

	err = client.Ping(context.Background(), nil)
	if err != nil {
		o.Logger.Error().Err(err).Msgf("MongoDB ping error: %s", err)
	}

	return client, nil
}

func (m *Model) Create(db *mongo.Database, collectionName string, model interface{}) error {
	collection := db.Collection(collectionName)

	m.CreatedAt = time.Now()
	m.UpdatedAt = time.Now()

	res, err := collection.InsertOne(context.Background(), model)
	if err != nil {
		return err
	}

	m.ID = res.InsertedID.(primitive.ObjectID)
	return nil
}

func (m *Model) Read(db *mongo.Database, collectionName string, filter interface{}, result interface{}) error {
	collection := db.Collection(collectionName)

	err := collection.FindOne(context.Background(), filter).Decode(result)
	if err != nil {
		return err
	}

	return nil
}

func (m *Model) Update(db *mongo.Database, collectionName string, filter interface{}, update interface{}) error {
	collection := db.Collection(collectionName)

	m.UpdatedAt = time.Now()

	_, err := collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		return err
	}

	return nil
}

func (m *Model) Delete(db *mongo.Database, collectionName string, filter interface{}) error {
	collection := db.Collection(collectionName)
	_, err := collection.DeleteOne(context.Background(), filter)
	if err != nil {
		return err
	}

	return nil
}
