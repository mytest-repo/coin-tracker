package model

type Symbol struct {
	Model
	Symbol          string `bson:"symbol"`
	Name            string `bson:"name"`
	BaseCurrency    string `bson:"baseCurrency"`
	QuoteCurrency   string `bson:"quoteCurrency"`
	FeeCurrency     string `bson:"feeCurrency"`
	Market          string `bson:"market"`
	BaseMinSize     string `bson:"baseMinSize"`
	QuoteMinSize    string `bson:"quoteMinSize"`
	BaseMaxSize     string `bson:"baseMaxSize"`
	QuoteMaxSize    string `bson:"quoteMaxSize"`
	BaseIncrement   string `bson:"baseIncrement"`
	QuoteIncrement  string `bson:"quoteIncrement"`
	PriceIncrement  string `bson:"priceIncrement"`
	PriceLimitRate  string `bson:"priceLimitRate"`
	MinFunds        string `bson:"minFunds"`
	IsMarginEnabled bool   `bson:"isMarginEnabled"`
	EnableTrading   bool   `bson:"enableTrading"`
}
