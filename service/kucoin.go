package service

import (
	event "cointracker/broker/rabbitmq"
	"cointracker/client/kucoin"
	"cointracker/model"
	"encoding/json"
	"fmt"
	"net/url"
	"os"
	"os/signal"
	"strings"
	"time"

	amqp "github.com/rabbitmq/amqp091-go"
	"github.com/rs/zerolog"
	"github.com/spf13/viper"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

const (
	_1min KlineInterval = iota
	_3min
	_5min
	_15min
	_30min
	_1hour
	_4hour
	_6hour
	_8hour
	_12hour
	_1day
	_1week
	//_1month
)

type KlineInterval int

func (i KlineInterval) String() string {
	return i.all()[i]
}

func (i KlineInterval) all() []string {
	return []string{
		"1min",
		"3min",
		"5min",
		"15min",
		"30min",
		"1hour",
		"4hour",
		"6hour",
		"8hour",
		"12hour",
		"1day",
		"1week",
		//"1month",
	}
}

type Ping struct {
	Id   int64  `json:"id"`
	Type string `json:"type"`
}

type Subscribe struct {
	Id             int64  `json:"id"`
	Type           string `json:"type"`
	Topic          string `json:"topic"`
	PrivateChannel bool   `json:"privateChannel"`
	Response       bool   `json:"response"`
}

type BaseMessage struct {
	Id   string `json:"id"`
	Type string `json:"type"`
}

type SubscribeMessage struct {
	BaseMessage
	Topic   string `json:"topic"`
	Subject string `json:"subject"`
	Data    struct {
		Symbol  string   `json:"symbol"`
		Candles []string `json:"candles"`
		Time    int64    `json:"time"`
	} `json:"data"`
}

type KucoinService struct {
	Logger *zerolog.Logger

	interrupt chan os.Signal

	rabbitConn *amqp.Connection
	database   *mongo.Database

	client            *kucoin.RestApi
	websocket         Websocket
	pingInterval      *time.Ticker
	websocketToken    string
	websocketEndpoint *url.URL
}

func (ks *KucoinService) Init() *KucoinService {
	l := zerolog.New(
		zerolog.ConsoleWriter{Out: os.Stdout, TimeFormat: time.DateTime},
	).Level(zerolog.InfoLevel).With().Caller().Timestamp().Logger()
	ks.Logger = &l

	ks.interrupt = make(chan os.Signal, 1)
	signal.Notify(ks.interrupt, os.Interrupt)

	ks.setDatabaseConnection()
	ks.setRabbitConnection()
	ks.client = new(kucoin.RestApi).Init()

	res, err := ks.client.GetWebsocketToken()
	if err != nil {
		panic(err)
	}
	r, _ := json.Marshal(res)
	ks.Logger.Info().Msgf("Get websocket token: %s", r)

	ks.setWebSocketEndpoint(res)

	interval := res.Data.InstanceServers[0].PingInterval
	ks.pingInterval = time.NewTicker(time.Millisecond * time.Duration(absInt(interval-100)))

	ks.setAndStoreSymbols()

	return ks
}

func (ks *KucoinService) Run() {
	ks.Logger.Info().Msgf("Run kucoin service")

	ks.connect()
}

func (ks *KucoinService) Subscribe(topic string) {
	ms := Subscribe{
		Id:             time.Now().UnixMilli(),
		Type:           "subscribe",
		Topic:          topic,
		PrivateChannel: false,
		Response:       true,
	}
	err := ks.websocket.WriteJSON(ms)
	if err != nil {
		ks.Logger.Error().Err(err).Msgf("error in subscribe: %s", topic)
	}
	ks.Logger.Info().Msgf("subscribe topic: %s", topic)
}

func (ks *KucoinService) Unsubscribe(topic string) {
	ms := Subscribe{
		Id:             time.Now().UnixMilli(),
		Type:           "unsubscribe",
		Topic:          topic,
		PrivateChannel: false,
		Response:       true,
	}
	err := ks.websocket.WriteJSON(ms)
	if err != nil {
		ks.Logger.Error().Err(err).Msgf("error in unsubscribe: %s", topic)
	}
	ks.Logger.Info().Msgf("unsubscribe topic: %s", topic)
}

func (ks *KucoinService) Ping() {
	ms := Ping{
		Id:   time.Now().UnixMilli(),
		Type: "ping",
	}
	err := ks.websocket.WriteJSON(ms)
	if err != nil {
		ks.Logger.Error().Err(err).Msgf("error in ping")
	}
}

func (ks *KucoinService) connect() {
	ks.websocket = Websocket{
		Verbose:   true,
		Reconnect: true,
		OnDisconnect: func(ws *Websocket) {
			ws.Logger.Warn().Msgf("Websocket disconnected")
		},
		OnConnect: func(ws *Websocket) {
			ws.Logger.Info().Msgf("Websocket connected")
			ks.subscriber()
		},
	}
	ks.websocket.Init()

	err := ks.websocket.Dial(ks.websocketEndpoint.String())
	if err != nil {
		ks.Logger.Error().Err(err).Msgf("Websocket connection dial error")
	}

	ks.receiveMessages()
	ks.pingSender()
}

func (ks *KucoinService) subscriber() {
	go func() {
		for _, symbol := range ks.getSymbols() {
			select {
			case <-time.After(time.Millisecond * viper.GetDuration("kucoin.subscribeDelay")):
				ks.Subscribe(ks.getAllResolutionTopic(symbol))
			case <-ks.interrupt:
				ks.Logger.Warn().Msgf("interrupt: %#v", ks.interrupt)
				ks.websocket.Close()
				return
			}
		}
	}()
}

func (ks *KucoinService) pingSender() {
	for {
		select {
		case <-ks.pingInterval.C:
			ks.Ping()
		case <-ks.interrupt:
			ks.Logger.Warn().Msgf("interrupt: %#v", ks.interrupt)
			ks.websocket.Close()
			return
		}
	}
}

func (ks *KucoinService) receiveMessages() {
	go func() {
		for {
			mt, message, err := ks.websocket.ReadMessage()
			if err != nil {
				ks.Logger.Error().Msgf("Receive error: %s , type: %d , message: %s", err, mt, message)
			} else {
				msg := new(BaseMessage)
				err := json.Unmarshal(message, &msg)
				if err != nil {
					ks.Logger.Warn().Msgf("message decode error: %s", msg)
				} else {
					switch msg.Type {
					case "message":
						ks.sendQueue(message)
					case "welcome":
					default:
						break
					}
				}

				ks.Logger.Info().Msgf("receive message: %s", message)
			}
		}
	}()
}

func (ks *KucoinService) setDatabaseConnection() {
	mongoConnectionString := fmt.Sprintf("mongodb://%s:%d",
		viper.GetString("mongodb.host"),
		viper.GetInt("mongodb.port"),
	)
	connection, err := new(model.ORM).Connect(mongoConnectionString)
	if err != nil {
		panic(err)
	}
	ks.database = connection.Database(viper.GetString("mongodb.database"))
	ks.Logger.Info().Msgf("Successfully connect to MongoDB")
}

func (ks *KucoinService) setRabbitConnection() {
	rabbitConnectionString := fmt.Sprintf("amqp://%s:%s@%s:%d",
		viper.GetString("rabbitmq.user"),
		viper.GetString("rabbitmq.password"),
		viper.GetString("rabbitmq.host"),
		viper.GetInt("rabbitmq.port"),
	)

	conn, err := amqp.Dial(rabbitConnectionString)
	if err != nil {
		ks.Logger.Error().Err(err).Msgf("Rabbitmq connection failed:")
		panic(err)
	}
	ks.rabbitConn = conn
	ks.Logger.Info().Msgf("Successfully connect to RabbitMQ")
}

func (ks *KucoinService) setAndStoreSymbols() {
	symbols, err := ks.client.GetAllSymbol()
	if err != nil {
		ks.Logger.Error().Err(err).Msgf("can't get symbols")
		panic(err)
	}
	ks.storeSymbols(symbols)
}

func (ks *KucoinService) getSymbols() []string {
	return []string{"BTC-USDT", "ETH-USDT", "BCH-USDT"}
}

func (ks *KucoinService) setWebSocketEndpoint(res kucoin.GetTokenRes) {
	u, err := url.Parse(res.Data.InstanceServers[0].Endpoint)
	if err != nil {
		panic(err)
	}
	query := u.Query()
	query.Add("token", res.Data.Token)
	query.Add("connectId", viper.GetString("kucoin.socketId"))
	u.RawQuery = query.Encode()

	ks.websocketEndpoint = u
	ks.websocketToken = res.Data.Token
}

func (ks *KucoinService) getAllResolutionTopic(symbol string) string {
	s := ""
	for i, k := range KlineInterval(0).all() {
		if i == 0 {
			s += fmt.Sprintf("/market/candles:%s_%s", symbol, k)
		} else {
			s += fmt.Sprintf(",%s_%s", symbol, k)
		}
	}

	return s
}

func (ks *KucoinService) sendQueue(message []byte) {
	msg := new(SubscribeMessage)
	err := json.Unmarshal(message, &msg)
	if err != nil {
		ks.Logger.Error().Err(err).Msgf("error when decode subscribe message")
	}

	emitter, err := event.NewEventEmitter(ks.rabbitConn)
	if err != nil {
		panic(err)
	}

	split := strings.Split(msg.Topic, ":")
	routingKey := strings.Replace(split[1], "_", ".", 1)

	err = emitter.Push(string(message), routingKey)
	if err != nil {
		ks.Logger.Error().Err(err).Msgf("error when send data to rabbitmq: %s", message)
	}
}

func (ks *KucoinService) storeSymbols(res kucoin.GetSymbolsRes) {
	for _, s := range res.Data {
		symbol := model.Symbol{
			Symbol:          s.Symbol,
			Name:            s.Name,
			BaseCurrency:    s.BaseCurrency,
			QuoteCurrency:   s.QuoteCurrency,
			FeeCurrency:     s.FeeCurrency,
			Market:          s.Market,
			BaseMinSize:     s.BaseMinSize,
			QuoteMinSize:    s.QuoteMinSize,
			BaseMaxSize:     s.BaseMaxSize,
			QuoteMaxSize:    s.QuoteMaxSize,
			BaseIncrement:   s.BaseIncrement,
			QuoteIncrement:  s.QuoteIncrement,
			PriceIncrement:  s.PriceIncrement,
			PriceLimitRate:  s.PriceLimitRate,
			MinFunds:        s.MinFunds,
			IsMarginEnabled: s.IsMarginEnabled,
			EnableTrading:   s.EnableTrading,
		}

		ks.findAndStoreSymbol(&symbol)
	}
}

func (ks *KucoinService) findAndStoreSymbol(symbol *model.Symbol) {
	var readSymbol model.Symbol
	exist := readSymbol.Read(ks.database, "symbols", bson.M{"symbol": symbol.Symbol}, &readSymbol)
	if exist == nil {
		return
	}

	err := symbol.Create(ks.database, "symbols", symbol)
	if err != nil {
		panic(err)
	}
}

func absInt(x int) int {
	return absDiffInt(x, 0)
}

func absDiffInt(x, y int) int {
	if x < y {
		return y - x
	}
	return x - y
}
